spellfile\_local.vim
====================

This plugin extends the default value of `'spellfile'` for file buffers to add
extra spelling word lists that are unique to each path and (if applicable)
filetype.  The `'spellfile'` setting for a file named `test.vim` might look
like this:

    spellfile=~/.vim/spell/en.utf-8.add
      ,~/.vim/spell/path/%home%user%test.vim.en.utf-8.add
      ,~/.vim/spell/filetype/vim.en.utf-8.add

Words can then be added to the file's word list with `2zg`, or to the
filetype's list with `3zg`.

License
-------

Copyright (c) [Tom Ryder][1].  Distributed under the same terms as Vim itself.
See `:help license`.

[1]: https://sanctum.geek.nz/
